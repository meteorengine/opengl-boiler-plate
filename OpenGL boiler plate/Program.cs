﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Glfw3;
using OpenGL;

namespace OpenGL_boiler_plate
{
    class Program
    {
        static void Main(string[] args)
        {
            Glfw.SetErrorCallback(FuckingErrorCallback);

            if (!Glfw.Init())
            {
                throw new Exception("Failed to initialize GLFW");
            }

            // Create GLFW window
            GlfwWindowPtr window = Glfw.CreateWindow(1280, 720, "Hello?",GlfwMonitorPtr.Null, GlfwWindowPtr.Null);
            // Enable the OpenGL context for the current window
            Glfw.MakeContextCurrent(window);

            while (!Glfw.WindowShouldClose(window))
            {
                // Poll GLFW window events
                Glfw.PollEvents();

                // If you press escape the window will close
                if (Glfw.GetKey(window, Key.Escape))
                {
                    Glfw.SetWindowShouldClose(window, true);
                }

                // Set OpenGL clear color to red
                Gl.ClearColor(1,0,0,1);

                // Clear the screen
                Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                // Swap the front and back buffer, displaying the scene
                Glfw.SwapBuffers(window);
            }

            // Finally, clean up Glfw, and close the window
            Glfw.Terminate();
        }

        private static void FuckingErrorCallback(GlfwError code, string desc)
        {
            throw new Exception(String.Format("Hello fucking world... if you see this the error callback is finally working... now the error:\n{0} {1}", code.ToString(), desc));
        }
    }
}
