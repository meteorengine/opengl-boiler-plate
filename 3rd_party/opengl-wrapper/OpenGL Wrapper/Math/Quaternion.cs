﻿using System.Numerics;

namespace OpenGL
{
    /// <summary>
    /// Extension methods for the Quaternion structure.
    /// </summary>
    public static class QuaternionExtensions
    {
        /// <summary>
        /// Convert this instance to an axis-angle representation.
        /// </summary>
        /// <returns>A Vector4 that is the axis-angle representation of this quaternion.</returns>
        public static Vector4 ToAxisAngle(this Quaternion q)
        {
            if (q.W > 1.0f)
                q = Quaternion.Normalize(q);

            float w = 2.0f * (float)System.Math.Acos(q.W); // angle
            float den = (float)System.Math.Sqrt(1.0 - q.W * q.W);
            if (den > 0.0001f)
            {
                return new Vector4(new Vector3(q.X, q.Y, q.Z) / den, w);
            }
            else
            {
                // This occurs when the angle is zero. 
                // Not a problem: just set an arbitrary normalized axis.
                return new Vector4(Vector3.UnitX, w);
            }
        }
    }
}
